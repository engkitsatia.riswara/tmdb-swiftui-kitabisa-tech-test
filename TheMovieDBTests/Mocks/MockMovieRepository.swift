//
//  MockMovieRepository.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 12/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

@testable
import TheMovieDBKitabisa

final class MockMovieRepository: MovieServiceProtocol {
	
	func fetchMovieListSuccessState(completion: @escaping (MovieResponse) -> Void) {
		self.fetchMovieList(by: .nowPlaying) { (result) in
			
			switch result {
			case .success(let response):
				completion(response)
			case .failure(_):
				return
			}
			
		}
	}
	
	func fetchMovieDetailsSuccessState(with id: Int, completion: @escaping (Movie) -> Void) {
		self.fetchMovieDetails(with: id) { (result) in
			
			switch result {
			case .success(let movie):
				completion(movie)
			case .failure(_):
				return
			}
			
		}
	}
	
	func fetchMovieList(by category: MovieCategories, completion: @escaping (Result<MovieResponse, MovieError>) -> Void) {
		
		let response = MovieResponse(
			results: [.init(id: 1,
							title: "dummy title 1",
							backdropPath: "dummy backdroppath 1",
							posterPath: "dummy poster 1",
							overview: "dummy overview 1",
							voteAverage: 2.2,
							voteCount: 20,
							runtime: 200,
							releaseDate: "11-07-2020")])
		
		let movieError = MovieError.noData
		
		completion(.success(response))
		completion(.failure(movieError))
	}
	
	func fetchMovieDetails(with movieId: Int, completion: @escaping (Result<Movie, MovieError>) -> Void) {
		
		let movie = Movie(
			id: movieId,
			title: "dummy movie title",
			backdropPath: "dummy movie backdropPath",
			posterPath: "dummy movie posterPath",
			overview: "dummy movie overview",
			voteAverage: 30,
			voteCount: 200,
			runtime: 200,
			releaseDate: "11-07-2020"
		)
		
		let movieError = MovieError.noData
		
		completion(.success(movie))
		completion(.failure(movieError))
		
	}
	
	
}
