//
//  MovieDetailViewModelTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 12/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable
import TheMovieDBKitabisa

class MovieDetailViewModelTest: XCTestCase {
	
	private var movieDetailViewModel: MovieDetailViewModel!
	private var mockMovieRepository: MockMovieRepository!

    override func setUp() {
		mockMovieRepository = MockMovieRepository()
		movieDetailViewModel = MovieDetailViewModel(movieRepository: mockMovieRepository)
    }

    override func tearDown() {
		mockMovieRepository = nil
		movieDetailViewModel = nil
    }

    func testExample() {
		mockMovieRepository.fetchMovieListSuccessState { (response) in
			XCTAssertTrue(!response.results.isEmpty)
		}
		
		movieDetailViewModel.getMovieDetails(with: 10)
		
		mockMovieRepository.fetchMovieDetailsSuccessState(with: 10) { (movie) in
			XCTAssertNotNil(movie)
		}
		
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
