//
//  MovieDetailViewTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable
import TheMovieDBKitabisa

class MovieDetailViewTest: XCTestCase {
	
	private var movieDetailView: MovieDetailView!

    override func setUp() {
		movieDetailView = MovieDetailView(movieId: 1)
    }

    override func tearDown() {
        movieDetailView = nil
    }

	func testMovieDetailView() {
		XCTAssertNotNil(movieDetailView.body)
	}

}
