//
//  FavoriteMovieViewTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable
import TheMovieDBKitabisa

class FavoriteMovieViewTest: XCTestCase {
	
	private var favoriteMovieView: FavoriteMovieView!
	private var favoriteMovieStorage: FavoritedMovieStorageModel!

    override func setUp() {
		favoriteMovieStorage = FavoritedMovieStorageModel.shared
        favoriteMovieView = FavoriteMovieView(favoritedMovieStorageModel: favoriteMovieStorage)
    }

    override func tearDown() {
		favoriteMovieView = nil
    }

	
	func testFavoriteMovieView() {
		
		favoriteMovieStorage.getAllFavoritedMovies()
		
		XCTAssertNotNil(favoriteMovieView.body)
		
	}
	
	func testStorageModelSave() {
		
		let movie = Movie(
			id: 100,
			title: "dummy movie title",
			backdropPath: "dummy movie backdropPath",
			posterPath: nil,
			overview: "dummy movie overview",
			voteAverage: 30,
			voteCount: 200,
			runtime: 200,
			releaseDate: "11-07-2020"
		)
		
		favoriteMovieStorage.saveAsFavorite(movie: movie)
		
		XCTAssertTrue(!favoriteMovieStorage.favoritedMovies.isEmpty)
		
	}
	
	func testStorageModelDelete() {
		
		let movie = Movie(
			id: 100,
			title: "dummy movie title",
			backdropPath: "dummy movie backdropPath",
			posterPath: "https://image.tmdb.org/t/p/w500/goEW6QqoFxNI2pfbpVqmXj2WXwd.jpg",
			overview: "dummy movie overview",
			voteAverage: 30,
			voteCount: 200,
			runtime: 200,
			releaseDate: "11-07-2020"
		)
		
		favoriteMovieStorage.saveAsFavorite(movie: movie)
		
		favoriteMovieStorage.deleteFromFavoritedMovies(id: 100)
		
		XCTAssertTrue(!favoriteMovieStorage.favoritedMovies.isEmpty)
	}
	
}
