//
//  FavoriteButtonTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 21/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
import SwiftUI
@testable
import TheMovieDBKitabisa

class FavoriteButtonTest: XCTestCase {
	
	var favoriteButton: FavoriteButton<Spacer>!
	var action: (() -> Void)?
	
	func testFavoriteButton() {
		
		favoriteButton = FavoriteButton(action: nil, label: nil)
		
		XCTAssertNotNil(favoriteButton.body)
	}

}
