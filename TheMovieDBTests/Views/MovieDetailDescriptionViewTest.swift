//
//  MovieDetailDescriptionViewTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable
import TheMovieDBKitabisa

class MovieDetailDescriptionViewTest: XCTestCase {
	
	private var movieDetailDescView: MovieDetailDescriptionView!
	private var titleHorizontalStackView: TitleHorizontalStackView!
	private var movie: Movie!

    override func setUp() {
		movie = Movie(
			id: 1,
			title: "title",
			backdropPath: "backdropPath",
			posterPath: "posterPath",
			overview: "overview",
			voteAverage: 1.1,
			voteCount: 1010,
			runtime: 1,
			releaseDate: "releaseDate"
		)
        movieDetailDescView = MovieDetailDescriptionView(movie: movie)
		titleHorizontalStackView = TitleHorizontalStackView(movie: movie)
    }

    override func tearDown() {
		movie = nil
		movieDetailDescView = nil
    }
	
	func testmovieDetailDescView() {
		
		XCTAssertNotNil(titleHorizontalStackView.body)
		XCTAssertNotNil(movieDetailDescView.body)
		
	}

}
