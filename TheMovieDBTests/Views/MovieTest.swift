//
//  MovieTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable import TheMovieDBKitabisa

class MovieTest: XCTestCase {
	
	private var movie: Movie!

    override func setUp() {
		movie = Movie(
			id: 1,
			title: "Howl's Moving Castle",
			backdropPath: nil,
			posterPath: nil,
			overview: "this is the overview",
			voteAverage: 8.5,
			voteCount: 1000,
			runtime: nil,
			releaseDate: nil
		)
    }

    override func tearDown() {
		movie = nil
    }

	func testNotNil() {
		
		XCTAssertNotNil(movie)
		XCTAssertNotNil(movie.posterURL)
	}

}
