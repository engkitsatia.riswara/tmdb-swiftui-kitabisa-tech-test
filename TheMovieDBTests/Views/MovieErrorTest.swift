//
//  MovieErrorTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
@testable
import TheMovieDBKitabisa

class MovieErrorTest: XCTestCase {
	
	private var movieError: MovieError!

	func testMovieError() {
		
		movieError = MovieError.apiError
		
		XCTAssertTrue(movieError.localizedDescription == "Failed to fetch data")
		XCTAssertNotNil(movieError.errorUserInfo)
	}

}
