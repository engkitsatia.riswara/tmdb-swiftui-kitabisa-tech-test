//
//  HeartButtonStyleTest.swift
//  TheMovieDBKitabisaTests
//
//  Created by Engkit Satia Riswara on 21/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import XCTest
import SwiftUI
@testable
import TheMovieDBKitabisa

class HeartButtonStyleTest: XCTestCase {
	
	var heartButtonStyle: HeartButtonStyle!
	
	func testHeartButtonStyle() {
		heartButtonStyle = HeartButtonStyle()
		
		XCTAssertNotNil(heartButtonStyle)
	}

}
