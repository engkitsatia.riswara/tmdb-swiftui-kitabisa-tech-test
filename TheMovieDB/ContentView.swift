//
//  ContentView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 08/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		MovieListContainer(movieCategory: .nowPlaying)
    }
}
