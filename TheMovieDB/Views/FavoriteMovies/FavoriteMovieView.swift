//
//  FavoriteMovieView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 11/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI
import UIKit

struct FavoriteMovieView: View {
	
	@ObservedObject var favoritedMovieStorageModel: FavoritedMovieStorageModel = FavoritedMovieStorageModel.shared
	
	private let imageWidth: CGFloat = UIScreen.screenWidth / 3
	private let movieDescriptionWidth: CGFloat = (UIScreen.screenWidth * 2/3) - 28
	
	var body: some View {
		
		VStack(alignment: .center) {
			
			if !self.favoritedMovieStorageModel.favoritedMovies.isEmpty {
				
				List {
					
					ForEach(self.favoritedMovieStorageModel.favoritedMovies) { movie in
						
						HStack(alignment: .center) {
							
							Image(uiImage: movie.image ?? UIImage())
								.resizable()
								.frame(width: UIScreen.screenWidth / 3 - 16, height: 200, alignment: .center)
								.aspectRatio(contentMode: .fill)
								.cornerRadius(9)
								.shadow(radius: 4)
							
							VStack(alignment: .leading) {
								
								Text(movie.title ?? "")
									.frame(width: self.movieDescriptionWidth, height: 20, alignment: .leading)
									.font(.title)
									.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
								Spacer()
								Text(movie.releaseDate ?? "")
									.frame(width: self.movieDescriptionWidth, height: 30, alignment: .leading)
									.font(.headline)
									.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
								Text(movie.description ?? "")
									.frame(width: self.movieDescriptionWidth, height: 150, alignment: .leading)
									.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
								Spacer()
							}.frame(width: (UIScreen.screenWidth * 2/3), height: 230, alignment: .leading)
							
						}.frame(width: UIScreen.screenWidth, height: 230, alignment: .leading)
					}
				}
				
			} else {
				Spacer()
				Image("heart")
				Spacer()
			}
			
		}.onAppear {
			self.favoritedMovieStorageModel.getAllFavoritedMovies()
		}.navigationBarTitle("Favorite Movies")
		
		
		
	}
	
}
