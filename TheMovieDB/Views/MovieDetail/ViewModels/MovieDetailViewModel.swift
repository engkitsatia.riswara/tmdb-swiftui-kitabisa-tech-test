//
//  MovieDetailViewModel.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 10/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

final class MovieDetailViewModel: ObservableObject {
	
	@Published var movie: Movie?
	@Published var error: NSError?
	
	private let movieRepository: MovieServiceProtocol
	
	init(movieRepository: MovieServiceProtocol = MovieRepository.shared) {
		self.movieRepository = movieRepository
	}
	
	func getMovieDetails(with movieId: Int) {
		
		self.movieRepository.fetchMovieDetails(with: movieId) { (result) in
			
			switch result {
			case .success(let movie):
				self.movie = movie
			case .failure(let error):
				self.error = error as NSError
			}
			
		}
		
	}
	
}
