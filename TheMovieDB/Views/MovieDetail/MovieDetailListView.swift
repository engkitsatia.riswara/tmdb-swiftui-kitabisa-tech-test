//
//  MovieDetailListView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 10/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieDetailListView: View {
	
	let movie: Movie
	
	var body: some View {
		
		HStack {
			
			VStack {
				MovieImageView(movie: movie).padding(EdgeInsets(top: 54, leading: 0, bottom: 0, trailing: 0))
				Spacer()
			}
			
			MovieDetailDescriptionView(movie: movie)
			
		}
		
	}
}
