//
//  MovieDetailView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieDetailView: View {
	
	let movieId: Int
	
	@ObservedObject var viewModel: MovieDetailViewModel = MovieDetailViewModel()
	
	var body: some View {
		
		ZStack(alignment: .top) {
			
			if viewModel.movie != nil {
				MovieDetailListView(movie: self.viewModel.movie!)
			}
			
		}.navigationBarTitle("", displayMode: .inline)
		.onAppear {
			self.viewModel.getMovieDetails(with: self.movieId)
		}
		
	}
	
}
