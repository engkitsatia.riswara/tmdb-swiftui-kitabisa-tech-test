//
//  MovieDetailDescriptionView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 10/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieDetailDescriptionView: View {
	
	let movie: Movie
	
	var body: some View {
		
		VStack(alignment: .leading) {
			
			TitleHorizontalStackView(movie: movie)
			Text("Overview").padding(EdgeInsets(top: 0, leading: 0, bottom: 8, trailing: 0))
			Text(movie.overview)
			Spacer()
		}
		
	}
}

struct TitleHorizontalStackView: View {
	
	var movie: Movie
	
	@ObservedObject var favoritedMovieStorageModel: FavoritedMovieStorageModel = FavoritedMovieStorageModel.shared
	
	var body: some View {
		
		HStack(alignment: .center) {
			
			VStack(alignment: .leading) {
				Text(movie.title).font(.title)
				Text(movie.releaseDate ?? "").font(.headline)
			}
			
			Spacer()
			
			FavoriteButton(action: {
				ThreadManager.runOnMainThread {
					self.favoritedMovieStorageModel.getAllFavoritedMovies()
					
					if self.favoritedMovieStorageModel.favoritedMovies.contains(where: { $0.id == self.movie.id }) {
						self.favoritedMovieStorageModel.deleteFromFavoritedMovies(id: self.movie.id)
						self.favoritedMovieStorageModel.getAllFavoritedMovies()
					} else {
						self.favoritedMovieStorageModel.saveAsFavorite(movie: self.movie)
						self.favoritedMovieStorageModel.getAllFavoritedMovies()
					}
				}
			}) {
				
				Image(self.favoritedMovieStorageModel.favoritedMovies.contains(where: { $0.id == self.movie.id }) ? "heartFilled" : "heartUnfilled").renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original))
			}
			
		}.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
		.onAppear {
				self.favoritedMovieStorageModel.getAllFavoritedMovies()
		}
		
	}
}


