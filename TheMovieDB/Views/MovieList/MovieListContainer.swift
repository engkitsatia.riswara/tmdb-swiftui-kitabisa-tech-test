//
//  MovieListContainer.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 08/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieListContainer: View {
	
	@State var isPresented: Bool = false
	@State var movieCategory: MovieCategories
	
	@ObservedObject var viewModel: MovieListViewModel = MovieListViewModel()
	
	var body: some View {
		
		VStack {
			
			if viewModel.movies != nil {
				MovieListView(movies: self.viewModel.movies ?? [], category: viewModel.movieCategory)
			} else {
				Spacer()
			}
			
			Button(action: {
				
				withAnimation {
					self.isPresented.toggle()
				}
				
			}) {
				Text("Category").font(.headline).foregroundColor(Color.white).frame(width: UIScreen.screenWidth, height: 50, alignment: .center).padding(.zero)
			}.background(Color.blue)
			
		}.onAppear(perform: {
			self.viewModel.loadMovies(by: .nowPlaying)
		})
		.sheet(isPresented: self.$isPresented, content: {
			CategoriesModal(isPresented: self.$isPresented, didFinishSelectingCategories: { category in
				self.viewModel.loadMovies(by: category)
			})
		})
		
	}
}

struct CategoriesModal: View {
	
	@Binding var isPresented: Bool
	
	var didFinishSelectingCategories: ((MovieCategories) -> Void)?
	
	var body: some View {
		
		List {
			
			ForEach(MovieCategories.allCases) { category in
				
				Button(action: {
					self.isPresented.toggle()
					self.didFinishSelectingCategories?(category)
				}) {
					Text(category.description)
				}
				
			}.frame(width: UIScreen.screenWidth, height: 50, alignment: .leading)
			
		}
		.padding(EdgeInsets(top: 8, leading: 0, bottom: 0, trailing: 0))
		
	}
	
}
