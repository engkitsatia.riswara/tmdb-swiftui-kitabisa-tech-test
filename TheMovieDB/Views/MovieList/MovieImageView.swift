//
//  MovieImageView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieImageView: View {
	
	let movie: Movie
	let favoritedMovie: FavoritedMovie? = nil
	@ObservedObject var imageDownloader = ImageDownloader()
	
	var body: some View {
		
		ZStack {
			
			if self.imageDownloader.image != nil {
				Image(uiImage: self.imageDownloader.image!)
					.resizable()
					.aspectRatio(contentMode: .fill)
					.cornerRadius(9)
					.shadow(radius: 4)
			} else {
				Rectangle()
                    .fill(Color.gray.opacity(0.3))
                    .cornerRadius(8)
                    .shadow(radius: 4)
                
                Text(movie.title)
                    .multilineTextAlignment(.center)
			}
			
			}.frame(width: UIScreen.screenWidth / 3 - 16, height: 100).padding(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 8))
        .onAppear {
			self.imageDownloader.downloadImage(with: self.movie.posterURL)
        }
	}
	
}
