//
//  MovieListView.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 08/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct MovieListView: View {
	
	private var movies: [Movie]
	private var category: MovieCategories
	
	private let imageWidth: CGFloat = UIScreen.screenWidth / 3
	private let movieDescriptionWidth: CGFloat = (UIScreen.screenWidth * 2/3) - 28
	
	init(movies: [Movie], category: MovieCategories) {
		self.movies = movies
		self.category = category
		UITableView.appearance().showsVerticalScrollIndicator = false
	}
	
	var body: some View {
		
		NavigationView {
			List {
				Group {
					
					Section(header: Text(self.category.description)) {
						
						ForEach(self.movies) { movie in
							
							NavigationLink(destination: MovieDetailView(movieId: movie.id)) {
								
								HStack {
									
									MovieImageView(movie: movie)
									
									VStack {
										
										Text(movie.title)
											.frame(width: self.movieDescriptionWidth, height: 20, alignment: .leading)
											.font(.title)
											.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
										Spacer()
										Text(movie.releaseDate!)
											.frame(width: self.movieDescriptionWidth, height: 30, alignment: .leading)
											.font(.headline)
											.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
										Text(movie.overview)
											.frame(width: self.movieDescriptionWidth, height: 150, alignment: .leading)
											.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
										Spacer()
										
										}
									.frame(width: (UIScreen.screenWidth * 2/3), height: 200, alignment: .leading)
								}.frame(width: UIScreen.screenWidth, height: 200, alignment: .leading)
							}
							
						}
						
					}
					.padding(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 8))
					
				}.listRowInsets(EdgeInsets(top: 16, leading: 0, bottom: 16, trailing: 0))
				
				}
			.navigationBarTitle(Text("TMDB App"))
			.navigationBarItems(trailing:
				NavigationLink(destination: FavoriteMovieView()) {
					Image("heart").renderingMode(Image.TemplateRenderingMode?.init(Image.TemplateRenderingMode.original))
				}
			)
		}
	}
}
