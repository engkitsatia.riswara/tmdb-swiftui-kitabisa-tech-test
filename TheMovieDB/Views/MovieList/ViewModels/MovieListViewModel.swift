//
//  MovieListViewModel.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

final class MovieListViewModel: ObservableObject {
	
	@Published var movies: [Movie]?
	@Published var error: NSError?
	@Published var category: String?
	@Published var movieCategory: MovieCategories
	
	private let movieRepository: MovieServiceProtocol

	init(repository: MovieServiceProtocol = MovieRepository.shared, category: MovieCategories = .nowPlaying) {
		self.movieRepository = repository
		self.movieCategory = category
	}
	
	func loadMovies(by category: MovieCategories) {
		self.movies = nil
		self.category = nil
		self.movieCategory = .nowPlaying
		
		self.movieRepository.fetchMovieList(by: category) { [weak self] (result) in
			guard let self = self else { return }
			
			self.category = category.description
			self.movieCategory = category
			
			switch result {
			case .success(let response):
				self.movies = response.results
			case .failure(let error):
				self.error = error as NSError
			}
		}
		
	}

}
