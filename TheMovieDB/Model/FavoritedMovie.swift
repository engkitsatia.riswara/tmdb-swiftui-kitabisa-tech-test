//
//  FavoritedMovie.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 10/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation
import UIKit

struct FavoritedMovie: Identifiable {
	
	let id: Int?
	let image: UIImage?
	let title: String?
	let releaseDate: String?
	let description: String?
	var favorite: Bool = false
	
}
