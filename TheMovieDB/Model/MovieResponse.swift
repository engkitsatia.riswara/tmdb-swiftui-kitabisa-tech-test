//
//  MovieResponse.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

struct MovieResponse: Decodable {
    
    let results: [Movie]
}
