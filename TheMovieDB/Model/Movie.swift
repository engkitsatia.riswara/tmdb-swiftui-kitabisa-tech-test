//
//  Movie.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 08/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

struct Movie: Decodable, Identifiable, Hashable {
    
    let id: Int
    let title: String
    let backdropPath: String?
    let posterPath: String?
    let overview: String
    let voteAverage: Double
    let voteCount: Int
    let runtime: Int?
    let releaseDate: String?
	var favorite: Bool? = nil
    
    var posterURL: URL {
        return URL(string: "https://image.tmdb.org/t/p/w500\(posterPath ?? "")")!
    }
    
}
