//
//  AppConstants.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

final class AppContants {
	
	static let apiKey: String = "91aa6199c2caf0e84be54b43f0bbca62"
	static let basePath: String = "https://api.themoviedb.org/3"
	
}
