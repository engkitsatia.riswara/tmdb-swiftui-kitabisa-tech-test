//
//  ThreadManager.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

final class ThreadManager {
	
	static func runOnMainThread(_ block: @escaping () -> Void) {
		
		if Thread.isMainThread {
            block()
            return
        }
		
        DispatchQueue.main.async(execute: block)
		
	}
	
}
