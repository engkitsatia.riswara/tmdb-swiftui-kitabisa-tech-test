//
//  FavoriteButton.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 20/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI

struct FavoriteButton<Label>: View where Label: View {
	
	private let action: (() -> Void)?
	private let label: (() -> Label)?
	
	@State var buttonStyle = HeartButtonStyle()
	
	init(action: (() -> Void)? = nil, label: (() -> Label)? = nil) {
        self.action = action
        self.label = label
    }
	
	var body: some View {
        Button(action: {
			self.buttonStyle.isSelected.toggle()
            self.action?()
        }) {
            label?()
        }
        .buttonStyle(buttonStyle)
    }
	
}

struct HeartButtonStyle: ButtonStyle {
	
	var isSelected = false
	
	func makeBody(configuration: Configuration) -> some View {
		configuration.label
		.padding()
		.foregroundColor(.white)
			.background(isSelected ? Image("heartFilled") : Image("heartUnfilled"))
		.cornerRadius(8.0)
	}
	
}
