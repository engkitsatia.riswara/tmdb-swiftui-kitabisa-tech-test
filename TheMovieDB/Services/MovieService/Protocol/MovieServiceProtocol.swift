//
//  MovieServiceProtocol.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

protocol MovieServiceProtocol {
	
	func fetchMovieList(by category: MovieCategories, completion: @escaping (Result<MovieResponse, MovieError>) -> Void)
	func fetchMovieDetails(with movieId: Int, completion: @escaping (Result<Movie, MovieError>) -> Void)
	
}

enum MovieCategories: String, CaseIterable, Identifiable {
	
	static var allCases: [MovieCategories] {
		return [.nowPlaying, .popular, .topRated, .upcoming]
	}
	
	var id: String { rawValue }
	
	case nowPlaying = "now_playing"
	case popular = "popular"
	case topRated = "top_rated"
	case upcoming = "upcoming"
	
	var description: String {
        switch self {
            case .nowPlaying: return "Now Playing"
            case .upcoming: return "Upcoming"
            case .topRated: return "Top Rated"
            case .popular: return "Popular"
        }
    }
	
}
