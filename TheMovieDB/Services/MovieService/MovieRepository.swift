//
//  MovieRepository.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation

class MovieRepository: MovieServiceProtocol {
	
	static let shared: MovieServiceProtocol = MovieRepository()
	private init() {}
	
	func fetchMovieList(by category: MovieCategories, completion: @escaping (Result<MovieResponse, MovieError>) -> Void) {
		
		guard let url = URL(string: "\(AppContants.basePath)/movie/\(category.rawValue)") else {
            completion(.failure(.invalidEndpoint))
            return
        }
		
		self.loadUrl(url: url, completion: completion)
		
	}
	
	func fetchMovieDetails(with movieId: Int, completion: @escaping (Result<Movie, MovieError>) -> Void) {
		
		guard let url = URL(string: "\(AppContants.basePath)/movie/\(movieId)") else {
            completion(.failure(.invalidEndpoint))
            return
        }
		
        self.loadUrl(url: url, params: [
            "append_to_response": "videos,credits"
        ], completion: completion)
		
	}
	
	private func loadUrl<D: Decodable>(url: URL, params: [String: String]? = nil, completion: @escaping (Result<D, MovieError>) -> ()) {
		guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
			completion(.failure(.invalidEndpoint))
            return
        }
		
		var queryItems = [URLQueryItem(name: "api_key", value: AppContants.apiKey)]
		if let params = params {
            queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
        }
		
		urlComponents.queryItems = queryItems
        
        guard let finalURL = urlComponents.url else {
			completion(.failure(.invalidEndpoint))
            return
        }
		
		let urlSession = URLSession.shared
		
		urlSession.dataTask(with: finalURL) { [weak self] (data, response, error) in
			guard let self = self else { return }
				
			guard error == nil else {
				self.executeOnMainThread(with: .failure(.apiError), completion: completion)
				return
			}
			
			guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                self.executeOnMainThread(with: .failure(.invalidResponse), completion: completion)
                return
            }
            
            guard let data = data else {
                self.executeOnMainThread(with: .failure(.noData), completion: completion)
                return
            }
			
			let dateFormatter: DateFormatter = DateFormatter()
			dateFormatter.dateFormat = "yyyy-mm-dd"
			let jsonDecoder: JSONDecoder = JSONDecoder()
			jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
			jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
			
			do {
				
				let response = try jsonDecoder.decode(D.self, from: data)
				
				self.executeOnMainThread(with: .success(response), completion: completion)
				
			} catch {
				
				self.executeOnMainThread(with: .failure(.serializationError), completion: completion)
				
			}
			
		}.resume()
		
	}
	
	private func executeOnMainThread<D: Decodable>(with result: Result<D, MovieError>, completion: @escaping (Result<D, MovieError>) -> Void) {
		
		ThreadManager.runOnMainThread {
			completion(result)
		}
		
	}
	
}
