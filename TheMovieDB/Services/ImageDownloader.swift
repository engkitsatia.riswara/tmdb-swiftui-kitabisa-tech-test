//
//  ImageDownloader.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 09/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import SwiftUI
import UIKit

private let _imageCache = NSCache<AnyObject, AnyObject>()

class ImageDownloader: ObservableObject {
	
	@Published var image: UIImage?
	
	var imageCache = _imageCache
	
	func downloadImage(with url: URL) {
		let urlString = url.absoluteString
		if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
		
		DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            do {
                let data = try Data(contentsOf: url)
                guard let image = UIImage(data: data) else {
                    return
                }
                self.imageCache.setObject(image, forKey: urlString as AnyObject)
				
				ThreadManager.runOnMainThread { [weak self] in
					self?.image = image
				}
				
            } catch {
                print(error.localizedDescription)
            }
        }
		
	}
	
}
