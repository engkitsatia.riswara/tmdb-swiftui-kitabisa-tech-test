//
//  FavoritedMovieStorageModel.swift
//  TheMovieDBKitabisa
//
//  Created by Engkit Satia Riswara on 10/07/20.
//  Copyright © 2020 Engkit Satia Riswara. All rights reserved.
//

import Foundation
import UIKit
import SQLite
import SwiftUI

final class FavoritedMovieStorageModel: ObservableObject {
	
	// MARK: - Public properties
	static let shared = FavoritedMovieStorageModel()
	
	@Published var favoritedMovies: [FavoritedMovie] = []
	
	// MARK: - Private properties
	private var connection: Connection?
	private var table: Table?
	private let tableName: String = "favorited_movies"
	
	private let movieId = Expression<Int?>("movieId")
	private let movieTitle = Expression<String?>("movieTitle")
	private let movieReleaseDate = Expression<String?>("movieReleaseDate")
	private let movieImage = Expression<Data?>("movieImage")
	private let movieDescription = Expression<String?>("movieDescription")
	private var favorite = Expression<Bool?>("favorite")
	
	// MARK: - Private
	private init() {
		try? prepare()
	}
	
	// MARK: SQLite configuration
	private func prepare() throws {
		try? createConnection()
		try? createTable()
	}
	
	private func createConnection() throws {
		let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
		
		guard let validPath = paths.first else {
			// TODO Engkit: Provide a better error handler object e.g ResponseError with descriptions
			throw NSError()
		}
		
		self.connection = try Connection("\(validPath)/\(tableName).sqlite3")
	}
	
	// MARK: - Table creation
	private func createTable() throws {
		self.table = Table(tableName)
		
		let query = table?.create(ifNotExists: true) { (builder: TableBuilder) in
			builder.column(movieId, unique: true)
			builder.column(movieTitle)
			builder.column(movieReleaseDate)
			builder.column(movieImage)
			builder.column(movieDescription)
			builder.column(favorite)
		}
		
		guard let validQuery = query else {
			throw NSError()
		}
		
		do {
			try connection?.run(validQuery)
		} catch {
			throw NSError()
		}
	}
	
	// MARK: - Save to local storage
	private func _saveAsFavorite(movie: Movie) throws {
		guard let connection = connection, let table = table else {
			// TODO Engkit: Provide a better error handler object e.g ResponseError with descriptions
			throw NSError()
		}
		
		do {
			let movieImageData = try Data(contentsOf: movie.posterURL)
			try connection.run(table.insert(
				self.movieId <- movie.id as Int,
				self.movieTitle <- movie.title as String,
				self.movieReleaseDate <- movie.releaseDate as String?,
				self.movieImage <- movieImageData as Data,
				self.movieDescription <- movie.overview as String,
				self.favorite <- true as Bool)
			)
		} catch let error {
			print(error.localizedDescription)
		}
	}
	
	// MARK: - Get from local storage
	private func _getMoviesFromStorage() throws -> [FavoritedMovie] {
		var movies: [FavoritedMovie] = []
		
		guard let connection = connection, let table = table else {
			// TODO Engkit: Provide a better error handler object e.g ResponseError with descriptions
			throw NSError()
		}
		
		for row in try connection.prepare(table) {
			
			do {
				
				guard let title = try row.get(movieTitle),
				let id = try row.get(movieId),
				let releaseData = try row.get(movieReleaseDate),
				let movieImage = try row.get(movieImage),
				let description = try row.get(movieDescription),
				let favorite = try row.get(favorite) else {
						return []
				}
				
				let image = UIImage(data: movieImage)
				
				let favoritedMovie = FavoritedMovie(id: id, image: image, title: title, releaseDate: releaseData, description: description, favorite: favorite)
				
				movies.append(favoritedMovie)
				
			} catch {
				
			}
			
		}
		
		return movies
	}
	
	// MARK: - Delete from local storage
	private func _deleteMovieFromStorage(movie: FavoritedMovie) throws {
		guard let connection = connection, let table = table else {
			// TODO Engkit: Provide a better error handler object e.g ResponseError with descriptions
			throw NSError()
		}
		
		do {
			
			let favMovie = table.filter(movieId == movie.id)
			
			try connection.run(favMovie.delete())
			
		} catch {
			print(error.localizedDescription)
		}
		
		
	}
	
	// MARK: - Find by id
	private func _findFavoritedMovie(by id: Int) throws -> FavoritedMovie? {
		
		let favoritedMovies = try _getMoviesFromStorage()
		
		let movies = favoritedMovies.filter { (movie) -> Bool in
			return movie.id == id
		}
		
		if !movies.isEmpty {
			return movies.first!
		} else {
			return nil
		}
		
	}
	
	// MARK: - Public
	// MARK: - Save favorited movies to local
	func saveAsFavorite(movie: Movie) {
		do {
			try _saveAsFavorite(movie: movie)
		} catch {
			print("Failed to save movie as favorite")
		}
	}
	
	func getAllFavoritedMovies() {
		
		do {
			let movies = try _getMoviesFromStorage()
			
			self.favoritedMovies = movies
		} catch let error {
			print(error.localizedDescription)
		}
	}
	
	func deleteFromFavoritedMovies(id: Int) {
		
		do {
			
			guard let favoritedMovie = try _findFavoritedMovie(by: id) else {
				return
			}
			
			try _deleteMovieFromStorage(movie: favoritedMovie)
			
		} catch {
			print(error.localizedDescription)
		}
		
	}
	
}
